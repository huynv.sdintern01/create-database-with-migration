<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $limit = 10;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('members')->insert([
                'login_fails' => $faker->numberBetween($min = 15, $max = 60),
            ]);
        }

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'password' => $faker->unique()->password,
                'email' => $faker->unique()->email,
                'login_fails' => $faker->numberBetween($min = 15, $max = 60),
                'member_id' => $faker->randomElement($array = array ('1','2','3')),
            ]);
        }
    }
}