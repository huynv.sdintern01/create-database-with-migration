<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_order_templates', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('name',191)->nullable();
            $table->bigInteger('price')->unsigned();
            $table->string('tourname_name', 191)->nullable();
            $table->text('tourname_explain');
            $table->string('tourname_image', 191)->nullable();
            $table->text('edit_history');
            $table->text('description');
            $table->json('concept')->nullable();
            $table->json('budget')->nullable();
            $table->json('note')->nullable();
            $table->text('contact_content');
            $table->string('contact_image', 500)->nullable();
            $table->string('public_url', 500)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->text('tourname_image_preview');
            $table->text('tourname_image_thumbnail');
            $table->text('contact_image_preview');
            $table->text('contact_image_thumbnail');
            $table->tinyInteger('type')->default(1)->comment('1: Normal, 2: Furusato.');
            $table->bigInteger('furusato_tour_city_id')->unsigned();
            $table->string('furusato_code', 191)->nullable();
            $table->text('furusato_from_site');
            $table->json('extra_info')->nullable();
            $table->tinyInteger('budget_remark_status')->default(4);
            $table->text('budget_remark');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_order_templates');
    }
};