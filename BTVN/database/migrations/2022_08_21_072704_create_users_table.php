<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('name', 191);
            $table->string('email', 191)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 191);
            $table->string('phone', 191)->nullable();
            $table->integer('role')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->bigInteger('creator_id')->unsigned()->nullable();
            $table->bigInteger('updater_id')->unsigned()->nullable();
            $table->string('tmp_password', 191)->nullable();
            $table->integer('login_fails')->default(0);
            $table->datetime('last_login_fail_at')->nullable();
            $table->datetime('last_login')->nullable();
            $table->string('profile_image', 500)->nullable();
            $table->string('remember_token', 100)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->bigInteger('member_id');
        });

        Schema::table('users', function(Blueprint $table)
        {
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};