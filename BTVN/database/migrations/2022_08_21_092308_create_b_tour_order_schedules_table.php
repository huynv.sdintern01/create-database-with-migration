<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_tour_order_schedules', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('b_tour_order_id');
            $table->string('title', 191)->nullable();
            $table->string('description', 191)->nullable();
            $table->date('date')->nullable();
            $table->json('detail')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::table('b_tour_order_schedules', function(Blueprint $table)
        {
            $table->foreign('b_tour_order_id')->references('id')->on('b_tour_orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_tour_order_schedules');
    }
};