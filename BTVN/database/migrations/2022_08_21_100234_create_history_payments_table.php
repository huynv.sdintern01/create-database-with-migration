<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_payments', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('member_id');
            $table->bigInteger('booking_id');
            $table->bigInteger('admin_id')->nullable();
            $table->double('amount')->nullable();
            $table->tinyInteger('status');
            $table->string('paypal_payment_id', 191)->nullable();
            $table->string('token', 191)->nullable();
            $table->string('payer_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        Schema::table('history_payments', function(Blueprint $table)
        {
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_payments');
    }
};