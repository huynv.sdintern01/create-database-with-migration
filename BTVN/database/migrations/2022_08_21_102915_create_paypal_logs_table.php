<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paypal_logs', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('member_id');
            $table->bigInteger('booking_id');
            $table->string('transaction_id', 191)->nullable();
            $table->string('sale_id', 191)->nullable();
            $table->string('amount', 191)->nullable();
            $table->text('transaction_log');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::table('paypal_logs', function(Blueprint $table)
        {
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypal_logs');
    }
};