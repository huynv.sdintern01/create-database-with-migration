<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_order_template_areas', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('tour_order_template_id');
            $table->bigInteger('prefecture_id');
            $table->bigInteger('city_id');
            $table->smallInteger('order_no')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::table('tour_order_template_areas', function(Blueprint $table)
        {
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('prefecture_id')->references('id')->on('prefectures')->onDelete('cascade');
            $table->foreign('tour_order_template_id')->references('id')->on('tour_order_templates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_order_template_areas');
    }
};