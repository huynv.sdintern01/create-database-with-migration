<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('sei', 191)->nullable();
            $table->string('mei', 191)->nullable();
            $table->string('sei_kana')->nullable();
            $table->string('mei_kana', 191)->nullable();
            $table->string('phone',191)->nullable();
            $table->string('email', 191)->nullable();
            $table->timestamp('birthday')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('address', 191)->nullable();
            $table->bigInteger('prefecture_id')->nullable();
            $table->bigInteger('city_id')->nullable();
            $table->string('street', 191)->nullable();
            $table->string('building', 191)->nullable();
            $table->string('user_type', 191)->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->string('introducer_name', 191)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 191)->nullable();
            $table->string('tmp_password', 191)->nullable();
            $table->datetime('last_login')->nullable();
            $table->integer('login_fails')->DEFAULT(0);
            $table->datetime('last_login_fail_at')->nullable();
            $table->string('remember_token', 100)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
};